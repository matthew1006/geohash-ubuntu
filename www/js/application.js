/*
 Copyright (C) 2016 Matthew Edwards <geohash@outlook.com>

 This file is part of geohash.

 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.

 This package is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see
 <http://www.gnu.org/licenses/>.
*/

function Application(UIContext) {
    this._uiContextClass = UIContext;
    this._initialized = false;
};

var DOW_URL = "http://geo.crox.net/djia/{year}/{month}/{day}";

var OSM_URL = "http://www.openstreetmap.org/export/embed.html?bbox={bb1},{bb2},{bb3},{bb4}&marker={lat},{lon}";
var OSM_SHARE_URL = "http://www.openstreetmap.org/?mlat={Latitude}&mlon={Longitude}#map=12/{Latitude}/{Longitude}";

var DJI = "";
var loc = "";
var user_loc = "";

var HASH_MULTIPLIER = 1/Math.pow(16,16);
var BORDER_OFFSET = 0.05;

var currentViewClass = "information modeButton";
var otherViewClass = "secondary modeButton";

var hashButton = 0;
var meButton = 0;

var activeView = "hash";
var errorDialog = 0;

Application.prototype.init = function() {
    if (this._uiContextClass && !this._initialized) {
        this._initialized = true;
        var UI = new this._uiContextClass();
        UI.init();

        UI.button("refresh-button").click(refresh);
        UI.button("share-button").click(shareLink);

        UI.button("about-button").click(function(){
            UI.pagestack.push ("about-page");
        });

        UI.sections = function(id) {
            if (typeof Sections != 'undefined' && Sections) {
                return new Sections(id);
            } else {
                console.error('Could not find the Sections element. You might be missing the "sections.js" Sections definition script. Please add a <script> declaration to include it.');
            }
        }

        var sections = UI.sections("location-selector");
        sections.onClicked (function(e) {
            activeView = e.values;
            updateMap();
        });

        UI.pagestack.push ("main-page");

        errorDialog = UI.dialog("error-dialog");
        UI.button ("error-dismiss").click (function () {
            errorDialog.hide ();
            if (errorDialog.callback)
                errorDialog.callback ();
        });

        setTimeout (refresh, 200);
    }
};

String.prototype.format = function(o)
{
    return this.replace(/{([^{}]+)}/g,
       function(a,b)
       {
           return o[b];
       }
    );
};

var getCoordsToUse = function ()
{
    switch (activeView)
    {
    case "hash":
        return loc;
    case "me":
        return user_loc;
    }
}

var formatToDegrees = function (coord, positive, negative) {

    var direction = positive;
    if (coord < 0)
    {
        direction = negative;
        coord = -coord;
    }

    var degrees = parseInt(coord);
    var remainder = (coord - degrees) * 60;
    var minutes = parseInt(remainder)
    remainder = (remainder - minutes) * 60;
    var seconds = parseInt(remainder);

    return "{deg}°&nbsp;{min}ʹ&nbsp;{sec}ʺ&nbsp;{dir}".format ({"deg":degrees, "min":minutes, "sec":seconds, "dir":direction});
}

var shareLink = function ()
{
    var api = external.getUnityObject('1.1');
    var hub = api.ContentHub;

    var filter = {
        "contentType":hub.ContentType.Links,
        "handler":hub.ContentHandler.Share
    };

    hub.launchContentPeerPicker (filter,
                 function(peer)
                {
                    peer.request (function(transfer)
                    {
                        transfer.setItems
                                (
                                    [
                                        {
                                            "name":"GeoHash",
                                            "url":getShareUrl()
                                        }
                                    ],
                                    function()
                                    {
                                        transfer.setState(hub.ContentTransfer.State.Charged);
                                    }
                                );
                    });
                },
                 function () {}
    );
}

var updateMap = function ()
{
    var location = getCoordsToUse();

    var mapFrame = document.getElementById("map-frame");
    mapFrame.src = getMapUrl(location.Latitude, location.Longitude);

    var latLabel = document.getElementById("latitude-label");
    var lonLabel = document.getElementById("longitude-label");

    latLabel.innerHTML = formatToDegrees(location.Latitude, "N", "S");
    lonLabel.innerHTML = formatToDegrees(location.Longitude, "E", "W");
}

var calculateHashCoords = function (latitude, longitude)
{
    var lat = parseInt(latitude);
    var lon = parseInt(longitude);

    var date = getDate();
    var dateDow = date.year + "-" + date.month + "-" + date.day + "-" + DJI;
    var hash = md5 (dateDow);

    var latHash = parseInt(hash.substring(0,16), 16) * HASH_MULTIPLIER;
    var lonHash = parseInt(hash.substring(16), 16) * HASH_MULTIPLIER;

    lat = parseFloat (lat + latHash.toString().substring(1));
    lon = parseFloat (lon + lonHash.toString().substring(1));

    loc = {"Latitude" : lat, "Longitude" : lon};

    return loc;
}

var getMapUrl = function (latitude, longitude)
{
    // Left, Bottom, Right, Top
    var bounds = [longitude-BORDER_OFFSET, latitude-BORDER_OFFSET,longitude+BORDER_OFFSET,latitude+BORDER_OFFSET];

    return OSM_URL.format (
                {
                    "bb1":bounds[0],
                    "bb2":bounds[1],
                    "bb3":bounds[2],
                    "bb4":bounds[3],
                    "lat":latitude,
                    "lon":longitude
                });
}

var getShareUrl = function ()
{
    return OSM_SHARE_URL.format (loc);
}

var getDate = function ()
{
    var d = new Date ();
    var year = d.getFullYear().toString();
    var month = (d.getMonth()+1).toString();
    if (month.length === 1)
        month = "0" + month;
    var day = d.getDate().toString();
    if (day.length === 1)
        day = "0" + day;

    return {"year":year, "month":month, "day":day};
}

var finance_charts_json_callback = function (data)
{
    var seriesCount = data["series"].length;
    DJI = data["series"][seriesCount-1]["open"] * 100;
    DJI = Math.floor(DJI);
    DJI *= 0.01;
    DJI = DJI.toString();

    navigator.geolocation.getCurrentPosition(GetLocation);
}

var refresh = function ()
{
    requestLocation ();

    //hashButton.element().className = "active";
    //meButton.element().className = "";
}

function getDJIA () {
    var dowRequest = new XMLHttpRequest ();

    dowRequest.onreadystatechange = function()
    {
        if (dowRequest.readyState == 4)
        {
            if (dowRequest.status == 200) {
                try
                {
                    DJI = parseFloat(dowRequest.responseText);
                    loc = calculateHashCoords (user_loc.Latitude, user_loc.Longitude);
                    console.log(JSON.stringify(loc))
                    updateMap();
                }
                catch (e)
                {
                    showError ("Error", "Error getting DJIA.", refresh);
                }
            }
            else
            {
                showError ("Network Error", "Could not connect to the internet.", refresh);
            }
        }
      };

    var date = getDate();

    if (user_loc.Latitude > -30)
    {
        date.day--;
    }

    dowRequest.open("GET", DOW_URL.format (date), true);
    dowRequest.send()
}

function requestLocation () {

    navigator.geolocation.getCurrentPosition(getLocationAndUpdateMap, function(error) {

        switch(error.code) {
                case error.PERMISSION_DENIED:
                    showError ("Location Error", "User denied the request for Geolocation.", requestLocation);
                    break;
                case error.POSITION_UNAVAILABLE:
                    showError ("Location Error", "Location information is unavailable.", requestLocation);
                    break;
                case error.TIMEOUT:
                    showError ("Location Error", "The request to get user location timed out.", requestLocation);
                    break;
                case error.UNKNOWN_ERROR:
                    showError ("Location Error", "An unknown error occurred.", requestLocation);
                    break;
            }
    });
}

function getLocationAndUpdateMap(location) {
    user_loc = {"Latitude":location.coords.latitude, "Longitude":location.coords.longitude};
    getDJIA();
}

Application.prototype.initialized = function() {
    return this._initialized;
};

var showError = function (title, message, callback) {
    if (errorDialog)
    {
        var errorTitle = document.getElementById ("error-title");
        var errorBody = document.getElementById ("error-body");

        errorTitle.innerHTML = title;
        errorBody.innerHTML = message;
        errorDialog.callback = callback;

        errorDialog.show ();
    }
}
