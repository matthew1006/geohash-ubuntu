from urllib.request import urlopen
from hashlib import md5
from webbrowser import open_new_tab as OpenBrowser
from time import localtime
from json import loads as ParseJSON

DECIMELISER = 5.421e-4
OSM_URL = "http://www.openstreetmap.org/?mlat="
DOW_URL = "http://chartapi.finance.yahoo.com/instrument/1.0/DJIA/chartdata;type=quote;range=1m/json/"

def GetDow ():
        data = urlopen (DOW_URL).read().decode()
        data = ParseJSON (data[30:-1])
        openValue = int(data["series"][-1]["open"] * 100)
        return str(openValue * 0.01)

def GetDate ():
        return "{:d}-{:02d}-{:02d}".format (*localtime())

def GetHash (date, dow):
        string = date + "-" + dow
        print(string)
        return md5 (string.encode()).hexdigest()

def main ():

        lat = float(input("Latitude: "))
        lon = float(input("Longitude: "))

        date = GetDate ()
        dow = GetDow ()
        digest = GetHash (date, dow)

        lat_tail = int(digest[:16], 16) * DECIMELISER
        lon_tail = int(digest[16:], 16) * DECIMELISER
        
        print(digest)
        print(lat_tail)
        print(lon_tail)

        geo_lat = "{:.0f}.{:.0f}".format (lat, lat_tail)
        geo_lon = "{:.0f}.{:.0f}".format (lon, lon_tail)

        OpenBrowser (OSM_URL + str(geo_lat) + "&mlon=" + str(geo_lon))

        return (geo_lat, geo_lon)

if __name__ == "__main__":
        print(main ())
